/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  assetPrefix: process.env.NODE_ENV === 'production' ? '/r6-quick-guide' : '',
  images: {
    loader: 'akamai',
    path: '',
  },
};

module.exports = nextConfig;
