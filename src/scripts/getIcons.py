import zipfile
import requests
from bs4 import BeautifulSoup
import urllib.request
import os 
import zipfile
from PIL import Image 

pwd = os.getcwd()
temp_folder = pwd + '/temp'
output_folder = pwd + '/opIcons'
png_folder = temp_folder + '/Scalable vector graphic (SVG)'
zip_archive = "operator_icons.zip"

def get_zip_archive_url() -> str:
    base_url = 'https://r6operators.marcopixel.eu'
    selector = '[download]'

    html = requests.get(base_url)
    soup = BeautifulSoup(html.text, "html.parser")
    link = soup.select_one(selector)

    return base_url + link.get('href')

def download_archive():
    archive_url = get_zip_archive_url()
    print("Downloading archive")
    urllib.request.urlretrieve(archive_url, zip_archive)
    print("Done")
    
    
def extract_archive():
    os.makedirs(temp_folder, exist_ok=True)
        
    with zipfile.ZipFile(zip_archive, 'r') as zip_ref:
        zip_ref.extractall(temp_folder)
        
    return
        
def get_filename(file_path: str) -> str:
    return os.path.splitext(file_path)[0]

def convert_image(image: str):
    output_file = output_folder + get_filename(image) + '.webp'
    img = Image.open(image)
    img = img.convert('RGB')
    img = img.resize((200,200))
    img.save(output_file, 'webp')
    return 

def convert_operator_icons():
    for file in os.listdir(png_folder):
        if file.endswith(".svg"):
            print(png_folder + '/' + file)
            convert_image(file)

        
if __name__ == "__main__":
    os.makedirs(output_folder, exist_ok=True)
    
    download_archive()
    extract_archive()
    convert_operator_icons()