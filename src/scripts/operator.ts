export enum Side {
  Attack = 'Attack',
  Defense = 'Defense',
}

const strToSide = (sideStr: string): Side =>
  sideStr === 'Attack' ? Side.Attack : Side.Defense;

export class Operator {
  name: string;
  description: string;
  side: Side;
  year: number;
  season: number;
  roles: string[];
  queer: string;

  constructor(
    name: string,
    description: string,
    sideStr: string,
    year: number,
    season: number,
    roles: string[],
    queer: string
  ) {
    this.name = name;
    this.description = description;
    this.side = strToSide(sideStr);
    this.year = year;
    this.season = season;
    this.roles = roles;
    this.queer = queer;
  }

  compareReleaseDate(operator: Operator, reversed: boolean) {
    const returnValue: number = reversed ? -1 : 1;

    if (this.year !== operator.year) {
      return this.year < operator.year ? -1 * returnValue : returnValue;
    }

    if (this.season !== operator.season) {
      return this.season < operator.season ? -1 * returnValue : returnValue;
    }

    if (this.side !== operator.side) {
      return this.side === Side.Attack ? returnValue : -1 * returnValue;
    }

    return this.compareAlphabetical(operator, true);
  }

  compareAlphabetical(operator: Operator, reversed: boolean) {
    const returnValue: number = this.name < operator.name ? -1 : 1;
    return reversed ? returnValue * -1 : returnValue;
  }

  isAttacker(): boolean {
    return this.side === Side.Attack;
  }

  isDefender(): boolean {
    return this.side === Side.Defense;
  }

  getFilename(): string {
    return this.name
      .normalize('NFD')
      .replace(/[\u0300-\u036f]/g, '')
      .replace('ø', 'o')
      .toLowerCase();
  }

  getImage(type: string): string {
    let folder: string;

    switch (type) {
      case 'Icon':
        folder = 'operatorIcons';
        break;
      case 'Card':
        folder = 'operatorCards';
        break;
      default:
        folder = '';
        console.error(`Unknown type: ${type}`);
        break;
    }

    const filename: string =
      this.name !== 'placeholder' ? this.getFilename() : 'ace';

    return `/${folder}/${filename}.webp`;
  }

  getRolesStr(): string {
    const nbRoles: number = this.roles.length;

    if (nbRoles === 0) {
      return '';
    }

    const prefixe: string = nbRoles > 1 ? 'Roles' : 'Role';
    return `${prefixe}: ${this.roles.join(', ')}`;
  }

  getClass(showQueer: boolean): string {
    let className: string = this.side === Side.Attack ? 'attacker' : 'defender';

    if (showQueer && this.queer) {
      className += ' queer';
    }

    return className;
  }
}

export const placeholderOperator = new Operator(
  'placeholder',
  'Select an operator to see their description.',
  Side.Attack,
  0,
  0,
  [],
  'gay'
);

export const filterSide = (operatorArr: Operator[], side: Side): Operator[] => {
  return side === Side.Attack
    ? operatorArr.filter((op) => op.isAttacker())
    : operatorArr.filter((op) => op.isDefender());
};

export const filterYear = (
  operatorArr: Operator[],
  yearArr: number[]
): Operator[] => {
  return operatorArr.filter((operator) => yearArr.includes(operator.year));
};

export const filterRoles = (
  operatorArr: Operator[],
  role: string
): Operator[] => {
  return operatorArr.filter((operator) => operator.roles.includes(role));
};

export const sortAlphabetical = (
  operatorArr: Operator[],
  reversed: boolean = false
): Operator[] => {
  return operatorArr.sort((a: Operator, b: Operator) => {
    return a.compareAlphabetical(b, reversed);
  });
};

export const sortReleaseDate = (
  operatorArr: Operator[],
  reversed: boolean = true
): Operator[] => {
  return operatorArr.sort((a: Operator, b: Operator) => {
    return a.compareReleaseDate(b, reversed);
  });
};
