import 'semantic-ui-css/semantic.min.css';
import '../styles/style.css';
import '../styles/card.css';
import '../styles/pride.css';
import '../styles/description.css';
import '../styles/filters.css';
import '../styles/loading.css';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}

export default MyApp;
