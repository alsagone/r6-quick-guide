import Head from 'next/head';
import {
  Operator,
  placeholderOperator,
  Side,
  filterSide,
  filterRoles,
  filterYear,
  sortAlphabetical,
  sortReleaseDate,
} from '../scripts/operator';
import { Description } from '../components/description';
import { OperatorCard } from '../components/operatorCard';
import React from 'react';
import legendData from '../../public/legend.json';
import { Button, Checkbox, Dropdown } from 'semantic-ui-react';

import Image from 'next/image';

interface Legend {
  Field: string;
  Color: string;
  TextColor: string;
}

interface Option {
  text: string;
  value: string;
  key: string;
}

type MyProps = {};

type MyState = {
  allOperators: Operator[];
  filteredOperators: Operator[];
  filterRoles: string[];
  filterRolesLabel: string;
  filterSide: Side[];
  filterYear: number[];
  filtered: boolean;
  loading: boolean;
  legendYear: Legend[];
  legendSide: Legend[];
  roleOptions: Option[];
  rolesStr: string;
  selectedOperator: Operator;
  selectedRole: string;
  showQueer: boolean;
  sortingMethod: string;
  showFilters: boolean;
  showMessage: string;
};

class App extends React.Component<MyProps, MyState> {
  constructor(props) {
    super(props);
    this.state = {
      allOperators: [] as Operator[],
      filteredOperators: [] as Operator[],
      filterRoles: [] as string[],
      filterRolesLabel: 'Role',
      filterSide: [Side.Attack, Side.Defense],
      filterYear: [],
      filtered: false,
      loading: false,
      legendYear: [] as Legend[],
      legendSide: [] as Legend[],
      roleOptions: [] as Option[],
      rolesStr: '',
      selectedOperator: placeholderOperator,
      selectedRole: '',
      showQueer: false,
      sortingMethod: 'Alphabetical',
      showFilters: false,
      showMessage: 'Show filters',
    };
  }

  componentDidMount() {
    this.fetchData();
    this.getLegend();
    this.getRoles();
  }

  fetchData = async function () {
    try {
      await fetch('r6-operators.json')
        .then((response) => {
          return response.json();
        })
        .then((data) => {
          for (const item of data) {
            const isQueer: string = item.Queer ? item.Queer : null;

            const newOperator: Operator = new Operator(
              item.Name,
              item.Description,
              item.Side,
              item.Year,
              item.Season,
              item.Roles,
              isQueer
            );

            this.setState({
              allOperators: [...this.state.allOperators, newOperator],
            });
          }
        });
    } catch (err: any) {
      alert(err.message);
    } finally {
      this.setState({
        allOperators: sortAlphabetical(
          this.state.allOperators,
          this.state.reversed
        ),
      });

      this.setState({ filteredOperators: [...this.state.allOperators] }, () => {
        this.getYears();
        this.filter();
        this.getRoles();
      });
    }
  };

  getLegend = (): void => {
    for (const item of legendData) {
      const field: string = item.Field;

      if (field.startsWith('Year')) {
        this.setState({ legendYear: [...this.state.legendYear, item] });
      } else if (field === 'Attackers' || field === 'Defenders') {
        this.setState({ legendSide: [...this.state.legendSide, item] });
      }
    }
  };

  getRoles = (): void => {
    const roles_set: Set<string> = new Set([]);

    for (const operator of this.state.filteredOperators) {
      for (const role of operator.roles) {
        roles_set.add(role);
      }
    }

    const rolesArr: Option[] = [];

    for (const role of Array.from(roles_set).sort()) {
      rolesArr.push({ text: role, value: role, key: role });
    }

    this.setState(() => ({
      roleOptions: rolesArr.sort((a, b) => a.text.localeCompare(b.text)),
    }));
  };

  getYears = (): void => {
    const yearSet: Set<number> = new Set<number>([]);

    for (const operator of this.state.filteredOperators) {
      yearSet.add(operator.year);
    }

    this.setState(() => ({
      filterYear: Array.from(yearSet),
    }));
  };

  sort = (): void => {
    this.setState((prevState) => ({
      filteredOperators:
        this.state.sortingMethod === 'Alphabetical'
          ? sortAlphabetical(prevState.filteredOperators)
          : sortReleaseDate(prevState.filteredOperators),
    }));
  };

  filterYear = (): void => {
    this.setState((prevState) => ({
      filteredOperators: filterYear(
        prevState.filteredOperators,
        prevState.filterYear
      ),
    }));
  };

  filterSide = (): void => {
    //Filter by side only if there's only one side selected
    if (this.state.filterSide.length === 1) {
      if (this.state.filterSide.includes(Side.Attack)) {
        this.setState((prevState) => ({
          filteredOperators: filterSide(
            prevState.filteredOperators,
            Side.Attack
          ),
        }));
      } else {
        this.setState((prevState) => ({
          filteredOperators: filterSide(
            prevState.filteredOperators,
            Side.Defense
          ),
        }));
      }
    }
  };

  filterRoles = (): void => {
    if (this.state.selectedRole.length > 0) {
      this.setState((prevState) => ({
        filteredOperators: filterRoles(
          prevState.filteredOperators,
          prevState.selectedRole
        ),
      }));
    }
  };

  filter = (): void => {
    this.setState((prevState) => ({
      filteredOperators: prevState.allOperators,
      loading: true,
    }));

    //Resets the selected operator if they aren't in the filtered array
    this.filterSide();
    this.filterYear();
    this.filterRoles();

    this.sort();
    this.changeSelectedOperator(this.state.selectedOperator);

    this.setState({ loading: false });
  };

  changeSelectedOperator = (operator: Operator) => {
    const previouslySelected = document.querySelector('.card.selected');
    const operatorDiv = document.querySelector(
      `#card-container > #${operator.name}`
    );

    if (previouslySelected) {
      previouslySelected.classList.remove('selected');
    }

    if (operatorDiv) {
      operatorDiv.classList.add('selected');
      this.setState({
        selectedOperator: operator,
      });
    } else {
      this.setState({
        selectedOperator: placeholderOperator,
      });
    }
  };

  changeYearFilter = (year: number) => {
    //Check if the current state contains the year
    if (this.state.filterYear.includes(year)) {
      this.setState(
        (prevState) => ({
          filterYear: prevState.filterYear.filter((y) => y !== year),
        }),
        () => this.filter()
      );
    } else {
      this.setState(
        (prevState) => ({
          filterYear: [...prevState.filterYear, year],
        }),
        () => this.filter()
      );
    }

    return;
  };

  getFieldColor = (field: string): string => {
    let color: string = '';
    let found = false;

    for (const element of legendData) {
      if (element.Field === field) {
        color = element.Color;
        found = true;
        break;
      }
    }

    if (!found) {
      throw new Error(`Legend element "${field} not found"`);
    }

    return color;
  };

  sideButtonOnClick = (side: Side) => {
    if (!this.state.filterSide.includes(side)) {
      this.setState(
        (prevState) => ({
          filterSide: [...prevState.filterSide, side],
        }),
        () => this.filter()
      );
    } else if (this.state.filterSide.length > 1) {
      this.setState(
        (prevState) => ({
          filterSide: prevState.filterSide.filter((s) => s !== side),
        }),
        () => this.filter()
      );
    }
  };

  changeSortingMethod = (method: string) => {
    this.setState(
      {
        sortingMethod: method,
      },
      () => this.sort()
    );
  };

  handleRoleChange = (value) => {
    this.setState(
      {
        selectedRole: value,
      },
      () => this.filter()
    );
  };

  displayFilters = () => {
    this.setState((prevState) => ({
      showFilters: !prevState.showFilters,
      showMessage: prevState.showFilters ? 'Show filters' : 'Hide filters',
    }));
  };

  checkboxChangeHandler = () => {
    this.setState((prevState) => ({
      showQueer: !prevState.showQueer,
    }));
  };

  render(): React.ReactNode {
    const loadingPanel = () => {
      if (this.state.loading) {
        return (
          <div id="loading">
            <div className="sk-folding-cube">
              <span className="sk-cube1 sk-cube" />
              <span className="sk-cube2 sk-cube" />
              <span className="sk-cube4 sk-cube" />
              <span className="sk-cube3 sk-cube" />
            </div>
            <span>Loading...</span>
          </div>
        );
      }
    };

    // sizes="(max-width: 768px) 20vw, 2vw"

    const intro: React.ReactNode = (
      <div id="intro">
        <div id="logo-container">
          <Image
            src="logo.webp"
            alt="Logo"
            layout="responsive"
            width={400}
            height={116}
            id="logo"
          />
        </div>
        <div id="credit">
          <span>
            Operator icons from{'  '}
            <a
              className="cool-link"
              href="https://r6operators.marcopixel.eu/"
              target="_blank"
              rel="noopener noreferrer"
            >
              r6operators.marcopixel.eu
            </a>
          </span>
          <span>
            Operator descriptions from the{'  '}
            <a
              className="cool-link"
              href="https://rainbowsix.fandom.com/wiki/Category:Rainbow_Operators"
              target="_blank"
              rel="noopener noreferrer"
            >
              R6 Wiki
            </a>
          </span>
          <span>
            Operator cards from the{'  '}
            <a
              className="cool-link"
              href="https://www.ubisoft.com/en-us/game/rainbow-six/siege/game-info/operators"
              target="_blank"
              rel="noopener noreferrer"
            >
              official R6 Website
            </a>
          </span>
          <span>
            Website by{'  '}
            <a
              className="cool-link"
              href="https://alsagone.gitlab.io/mentor-card"
              target="_blank"
              rel="noopener noreferrer"
            >
              @alsagone
            </a>
          </span>
        </div>
      </div>
    );

    const yearFilters: React.ReactNode = (
      <Button.Group>
        {legendData.map((item) => {
          if (item.Field.startsWith('Year')) {
            const match = item.Field.match(/(\d+)/);
            const yearNumber: number = match ? parseInt(match[0]) : 0;
            return (
              <Button
                style={{
                  backgroundColor: item.Color,
                  color: item.TextColor,
                }}
                key={item.Field}
                active={this.state.filterYear.includes(yearNumber)}
                onClick={() => this.changeYearFilter(yearNumber)}
              >
                {item.Field}
              </Button>
            );
          }
        })}
      </Button.Group>
    );

    const sideFilters: React.ReactNode = (
      <Button.Group>
        <Button
          style={{
            backgroundColor: '#d9610f',
            color: '#ffffff',
          }}
          active={this.state.filterSide.includes(Side.Attack)}
          onClick={() => this.sideButtonOnClick(Side.Attack)}
        >
          Attackers
        </Button>

        <Button
          style={{
            backgroundColor: '#0e88c9',
            color: '#ffffff',
          }}
          active={this.state.filterSide.includes(Side.Defense)}
          onClick={() => this.sideButtonOnClick(Side.Defense)}
        >
          Defenders
        </Button>
      </Button.Group>
    );

    const checkboxQueer: React.ReactNode = (
      <Checkbox
        toggle
        label="Show canon queer characters 🏳️‍🌈"
        name="queerCharacters"
        checked={this.state.showQueer}
        onChange={this.checkboxChangeHandler}
      />
    );

    const sortingButtons: React.ReactNode = (
      <div className="filterButtons">
        <Button.Group>
          <Button
            active
            positive={this.state.sortingMethod === 'Alphabetical'}
            onClick={() => this.changeSortingMethod('Alphabetical')}
          >
            Alphabetical
          </Button>
          <Button.Or />
          <Button
            active
            positive={this.state.sortingMethod === 'Release date'}
            onClick={() => this.changeSortingMethod('Release date')}
          >
            Release date
          </Button>
        </Button.Group>
      </div>
    );

    const rolesDropdown = (
      <Dropdown
        className="rolesDropdown"
        placeholder="Role"
        fluid
        clearable
        search
        selection
        options={this.state.roleOptions}
        value={this.state.selectedRole}
        onChange={(e, { value }) => this.handleRoleChange(value)}
      />
    );

    const cardContainer: React.ReactNode = (
      <div id="card-container">
        {this.state.filteredOperators.map((op: Operator) => {
          return (
            <OperatorCard
              key={op.name}
              operator={op}
              onClick={() => this.changeSelectedOperator(op)}
              color={this.getFieldColor(`Year ${op.year}`)}
              showQueer={this.state.showQueer}
            />
          );
        })}
      </div>
    );

    return (
      <div className="v-application">
        <Head>
          <title>R6 Quick Operator Guide</title>
          <meta
            name="viewport"
            content="initial-scale=1.0, width=device-width"
            charSet="utf-8"
          />
          <meta
            name="description"
            content="Website where you can get a quick explaination of how every operator works in Rainbow Six Siege"
          />
          <link rel="icon" href="/r6-quick-guide/favicon.ico" />
        </Head>
        <div id="background-image" />

        <div id="container">
          {loadingPanel()}
          {intro}
          <Button
            onClick={this.displayFilters}
            id="displayButton"
            className="positive active"
          >
            {this.state.showMessage}
          </Button>
          <div
            id="filters"
            className={this.state.showFilters ? 'visible' : 'hidden'}
          >
            <div id="buttonFilters">
              <div className="filterDiv">
                <span className="filterLabel">Side</span>
                <div className="filterButtons">{sideFilters}</div>
              </div>

              <div className="filterDiv">
                <span className="filterLabel">Year of release</span>
                <div className="filterButtons">{yearFilters}</div>
              </div>

              <div className="filterDiv">{checkboxQueer}</div>
            </div>

            <div id="advancedFilters">
              <div className="filterDiv">{rolesDropdown}</div>
              <div className="filterDiv" id="sortingMethod">
                <span className="filterLabel">Sorting method</span>

                {sortingButtons}
              </div>
            </div>
          </div>
          {cardContainer}
          <Description operator={this.state.selectedOperator} />
        </div>
      </div>
    );
  }
}

export default App;
