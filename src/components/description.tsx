import Image from 'next/image';
import React from 'react';
import { Operator } from '../scripts/operator';

type Props = {
  operator: Operator;
};

export class Description extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render(): React.ReactNode {
    const icon: React.ReactNode = (
      <span id="description-icon">
        <Image
          className="operator-icon"
          src={this.props.operator.getImage('Icon')}
          alt={this.props.operator.name}
          width="90"
          height="90"
          layout="fixed"
        />
        <span className="name">{this.props.operator.name}</span>
      </span>
    );

    return (
      <div id="description">
        {this.props.operator.name !== 'placeholder' && icon}
        <span id="description-text">
          {this.props.operator.description} <br />
          <br /> {this.props.operator.getRolesStr()}
        </span>
      </div>
    );
  }
}
