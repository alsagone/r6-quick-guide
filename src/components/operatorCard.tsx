import Image from 'next/image';
import React from 'react';
import { Operator } from '../scripts/operator';

type Props = {
  operator: Operator;
  color: string;
  showQueer: boolean;
  onClick?: (e: any) => void;
};

export class OperatorCard extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  getPridePlag = () => {
    if (!this.props.operator.queer) {
      return <></>;
    }

    let spanClass = `pride-flag ${this.props.operator.queer.toLowerCase()}`;

    if (this.props.showQueer) {
      spanClass += ' show-queer';
    }

    return <span className={spanClass}></span>;
  };

  render() {
    const yearStyle = {
      color: this.props.color,
    };

    return (
      <div
        className={`card ${this.props.operator.getClass(this.props.showQueer)}`}
        id={this.props.operator.name}
        key={this.props.operator.name}
        onClick={this.props.onClick}
      >
        <span className="operator-card">
          <Image
            src={this.props.operator.getImage('Card')}
            alt={this.props.operator.name}
            width="300"
            height="500"
          />
        </span>

        <span className="operator-icon">
          <Image
            src={this.props.operator.getImage('Icon')}
            alt={this.props.operator.name}
            width="95"
            height="95"
            layout="fixed"
          />
        </span>
        <span className="operator-year" style={yearStyle}>
          Y{this.props.operator.year}
        </span>
        {this.getPridePlag()}
        <span className="operator-name">{this.props.operator.name}</span>
      </div>
    );
  }
}
