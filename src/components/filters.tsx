import React from "react";
import { Operator } from "../scripts/operator";

type Props = {
  onClick?: (e: any) => void;
};

export class Filters extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  render() {
    return <div id="filters"></div>;
  }
}
