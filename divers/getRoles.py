from bs4 import BeautifulSoup
import requests 
import json 
import unidecode
import time


def getRoles(operator_name: str) -> list[str]:
    
    url = 'https://www.ubisoft.com/en-us/game/rainbow-six/siege/game-info/operators/' + operator_name.lower()
    
    headers = {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'GET',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Max-Age': '3600',
        'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'
    }
    req = requests.get(url, headers=headers)
    soup = BeautifulSoup(req.content, 'html.parser')
    
    search = soup.select("span.operator__header__roles > span")
    roles = [element.text for element in search]
    
    print(f'{operator_name} -> {", ".join(roles)}')
    
    return roles

def build_json():
    with open("r6-operators.json", 'r', encoding='utf-8') as f:
        data = json.load(f)
        

    for i in range(len(data)):
        name = unidecode.unidecode(data[i]["Name"])
        roles = getRoles(name)
        data[i]["Roles"] = roles 
        time.sleep(1)
        
    with open("test.json", "w+", encoding='utf-8') as f:
        json.dump(data, f, indent=4)

if __name__ == "__main__": 
    build_json()